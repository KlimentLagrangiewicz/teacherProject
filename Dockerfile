FROM openjdk:11
COPY target/*.jar teacher-0.0.1.jar
ENTRYPOINT ["java","-jar","/teacher-0.0.1.jar"]