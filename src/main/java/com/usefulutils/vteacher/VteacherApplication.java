package com.usefulutils.vteacher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VteacherApplication {

    public static void main(String[] args) {
        SpringApplication.run(VteacherApplication.class, args);
    }

}
