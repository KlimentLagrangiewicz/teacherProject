package com.usefulutils.vteacher.repository;

import com.usefulutils.vteacher.model.entity.ExamType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExamTypeRepository extends JpaRepository<ExamType, Long> {
}
