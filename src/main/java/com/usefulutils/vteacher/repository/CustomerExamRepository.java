package com.usefulutils.vteacher.repository;

import com.usefulutils.vteacher.model.entity.Customer;
import com.usefulutils.vteacher.model.entity.CustomerExam;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerExamRepository extends JpaRepository<CustomerExam, Long> {


    List<CustomerExam> findAllByCustomer(Customer customer);
}
