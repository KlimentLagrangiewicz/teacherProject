package com.usefulutils.vteacher.repository;

import com.usefulutils.vteacher.model.entity.CustomerTask;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CustomerTaskRepository extends JpaRepository<CustomerTask, Long> {

}
