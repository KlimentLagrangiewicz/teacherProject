package com.usefulutils.vteacher.repository;

import com.usefulutils.vteacher.model.entity.SimpleTask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SimpleTaskRepository<T extends SimpleTask> extends JpaRepository<T, Long> {

    public List<SimpleTask> findAllByCategory(String category);



}
