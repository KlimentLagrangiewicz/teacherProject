package com.usefulutils.vteacher.service;

import com.usefulutils.vteacher.model.entity.*;
import com.usefulutils.vteacher.repository.CustomerExamRepository;
import com.usefulutils.vteacher.repository.CustomerTaskRepository;
import com.usefulutils.vteacher.repository.SimpleTaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Service("examService")
public class ExamService {

    private static Random random = new Random();

    @Autowired
    TaskService taskService;

    @Autowired
    SimpleTaskRepository simpleTaskRepository;

    @Autowired
    CustomerTaskRepository customerTaskRepository;

    @Autowired
    CustomerExamRepository customerExamRepository;


    public CustomerExam generateCustomerExam(ExamType examType, Customer customer){
        List<SimpleTask> simpleChosenTaskList = (List<SimpleTask>) simpleTaskRepository.findAll().stream().filter(simpleTask -> random.nextBoolean()).collect(Collectors.toList());
        List<CustomerTask> customerTaskList = new ArrayList<>();
        System.out.println("sss" +  simpleChosenTaskList);
        CustomerExam customerExam = new CustomerExam(
                customerTaskList,
                new Date(System.currentTimeMillis()+examType.getExamTimeMinutes()*60*1000),
                null,
                null,
                customer,
                examType);


        System.out.println("sss" +  customerExam.getId());
        customerTaskList = simpleChosenTaskList.stream().map(simpleTask -> new CustomerTask(simpleTask,customerExam)).collect(Collectors.toList());
        System.out.println("sss" +  customerTaskList);
        customerExam.setListTask(customerTaskList);
        customerExamRepository.save(customerExam);

        return customerExam;
    };

    public Integer calculatePoints(List<CustomerTask> customerTasks){
        Integer points = 0;
        for(CustomerTask customerTask : customerTasks){
            if(customerTask.getCheckAnswer())
                points+=customerTask.getTask().getCost();
        }
      return points;
    };


}
