package com.usefulutils.vteacher.service;

import com.usefulutils.vteacher.model.entity.SimpleTask;
import com.usefulutils.vteacher.repository.SimpleTaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service("taskService")
public class TaskService {

    @Autowired
    SimpleTaskRepository simpleTaskRepository;

    public List<SimpleTask> getStringTasks(){
        return simpleTaskRepository.findAllByCategory("string");
    }

    public List<SimpleTask> getVariantTasks(){
        return simpleTaskRepository.findAllByCategory("variant");
    }
}
