package com.usefulutils.vteacher.controller;

import com.usefulutils.vteacher.model.entity.*;
import com.usefulutils.vteacher.model.response.TaskListResponse;
import com.usefulutils.vteacher.repository.CustomerExamRepository;
import com.usefulutils.vteacher.repository.CustomerRepository;
import com.usefulutils.vteacher.repository.CustomerTaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.QueryAnnotation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping(value = "customerTask")
public class CustomerTaskController {

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    CustomerTaskRepository customerTaskRepository;

    @Autowired
    CustomerExamRepository customerExamRepository;

    @Transactional
    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public ResponseEntity<TaskListResponse> getTask(@PathVariable Long id) {

        CustomerTask customerTask = customerTaskRepository.findById(id).orElse(null);
        if(customerTask == null)
            return new ResponseEntity<TaskListResponse>(new TaskListResponse( null, null, "error", "Task with id %d doesn't exists"), HttpStatus.I_AM_A_TEAPOT);
        return new ResponseEntity<TaskListResponse>(new TaskListResponse( null, List.of(customerTask), "success", "Task was created"), HttpStatus.OK);
    }

    @Transactional
    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public ResponseEntity<TaskListResponse> deleteTask(@PathVariable Long id) {
        CustomerTask customerTask = customerTaskRepository.findById(id).orElse(null);
        if(customerTask == null)
            return new ResponseEntity<TaskListResponse>(new TaskListResponse( null, null, "error", "Task with id %d doesn't exists"), HttpStatus.I_AM_A_TEAPOT);
        else{
            customerTaskRepository.delete(customerTask);
            return new ResponseEntity<TaskListResponse>(new TaskListResponse(null, List.of(customerTask), "success", "Task was deleted"), HttpStatus.OK);
        }
    }

    @Transactional
    @RequestMapping(value = "exam/{id}", method = RequestMethod.GET)
    public ResponseEntity<TaskListResponse> getCustomerExamTasks(@PathVariable Long id) {


        CustomerExam customerExam = customerExamRepository.findById(id).orElse(null);
            if(customerExam == null)
                return new ResponseEntity<TaskListResponse>(new TaskListResponse( null, null, "error", "Exam with id %d doesn't exists"), HttpStatus.I_AM_A_TEAPOT);

        List<CustomerTask> customerTasks = customerExam.getListTask();

        return new ResponseEntity<TaskListResponse>(new TaskListResponse( null, customerTasks, "success", ""), HttpStatus.OK);
    }

    @Transactional
    @RequestMapping(value = "exam/customer", method = RequestMethod.GET)
    public ResponseEntity<TaskListResponse> getCustomerExamTasksByCustomerLogin(@RequestParam(value="login", required =true) String login) {

        Customer customer = customerRepository.findById(login).orElse(null);
        if(customer == null)
            return new ResponseEntity<TaskListResponse>(new TaskListResponse( null, null, "error", "Customer with login %d doesn't exists"), HttpStatus.I_AM_A_TEAPOT);

        List<CustomerExam> customerExams = customerExamRepository.findAllByCustomer(customer);
        if(customerExams == null || customerExams.isEmpty())
            return new ResponseEntity<TaskListResponse>(new TaskListResponse( null, null, "error", "Exam with id %d doesn't exists"), HttpStatus.I_AM_A_TEAPOT);

        List<CustomerTask> customerTasks = customerExams.stream().flatMap(customerExam -> customerExam.getListTask().stream()).collect(Collectors.toList());

        return new ResponseEntity<TaskListResponse>(new TaskListResponse( null, customerTasks, "success", ""), HttpStatus.OK);
    }

    @Transactional
    @RequestMapping(value = "exam/customerExam", method = RequestMethod.GET)
    public ResponseEntity<TaskListResponse> getCustomerExamTaskByCustomerLogin(
            @RequestParam(value="login", required =true) String login,
            @RequestParam(value="examId", required =true) Long examId) {

        Customer customer = customerRepository.findById(login).orElse(null);
        if(customer == null)
            return new ResponseEntity<TaskListResponse>(new TaskListResponse( null, null, "error", "Customer with login %d doesn't exists"), HttpStatus.I_AM_A_TEAPOT);

        List<CustomerExam> customerExams = customerExamRepository.findAllByCustomer(customer);
        if(customerExams == null || customerExams.isEmpty())
            return new ResponseEntity<TaskListResponse>(new TaskListResponse( null, null, "error", "Exam with id %d doesn't exists"), HttpStatus.I_AM_A_TEAPOT);

        List<CustomerTask> customerTasks = customerExams.stream().filter(customerExam -> customerExam.getId().equals(examId)).flatMap(customerExam -> customerExam.getListTask().stream()).collect(Collectors.toList());

        return new ResponseEntity<TaskListResponse>(new TaskListResponse( null, customerTasks, "success", ""), HttpStatus.OK);
    }
}
