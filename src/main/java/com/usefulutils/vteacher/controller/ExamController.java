package com.usefulutils.vteacher.controller;

import com.usefulutils.vteacher.model.entity.Customer;
import com.usefulutils.vteacher.model.entity.CustomerExam;
import com.usefulutils.vteacher.model.entity.ExamType;
import com.usefulutils.vteacher.model.entity.StringTask;
import com.usefulutils.vteacher.model.response.CustomerResponse;
import com.usefulutils.vteacher.model.response.ExamResponse;
import com.usefulutils.vteacher.model.response.TaskListResponse;
import com.usefulutils.vteacher.repository.CustomerExamRepository;
import com.usefulutils.vteacher.repository.CustomerRepository;
import com.usefulutils.vteacher.repository.ExamTypeRepository;
import com.usefulutils.vteacher.service.ExamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("exam")
public class ExamController {

    @Autowired
    ExamService examService;

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    CustomerExamRepository customerExamRepository;

    @Autowired
    ExamTypeRepository examTypeRepository;

    @Transactional
    @RequestMapping(value = "generate", method = RequestMethod.POST)
    public ResponseEntity<ExamResponse> generate(
            @RequestParam(value="login", required =true) String login,
            @RequestParam(value="examTypeId", required =true) Long examTypeId){

        Customer customer = customerRepository.findById(login).orElse(null);

        if(customer==null){
            return new ResponseEntity<ExamResponse>(new ExamResponse( "error",String.format("Can't find customer with login %s", login), null), HttpStatus.I_AM_A_TEAPOT);
        }

        ExamType examType = examTypeRepository.findById(examTypeId).orElse(null);
        if(examType==null)
            examType = examTypeRepository.findById(1L).orElse(null);
        CustomerExam customerExam = examService.generateCustomerExam(examType, customer);

        return new ResponseEntity<ExamResponse>(new ExamResponse("success", "Exam was started",customerExam), HttpStatus.OK);
    }

}
