package com.usefulutils.vteacher.controller;

import com.usefulutils.vteacher.model.entity.SimpleTask;
import com.usefulutils.vteacher.model.entity.StringTask;
import com.usefulutils.vteacher.model.entity.VariantsTask;
import com.usefulutils.vteacher.model.response.TaskListResponse;
import com.usefulutils.vteacher.model.response.TaskResponse;
import com.usefulutils.vteacher.repository.SimpleTaskRepository;
import com.usefulutils.vteacher.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping(value = "task")
public class TaskController {

    @Autowired
    SimpleTaskRepository simpleTaskRepository;

    @Autowired
    TaskService taskService;

    

    @Transactional
    @RequestMapping(value = "create/string-task", method = RequestMethod.POST)
    public ResponseEntity<TaskResponse> createStringTask(
            @RequestParam(value="question", required =true) String question,
            @RequestParam(value="answer", required =true) String answer,
            @RequestParam(value="cost", required =false) Integer cost
            ){

        StringTask stringTask = new StringTask(question,  answer,  cost);
        simpleTaskRepository.save(stringTask);

        return new ResponseEntity<TaskResponse>(new TaskResponse( "success", "Task was created", stringTask), HttpStatus.OK);
    }

    @Transactional
    @RequestMapping(value = "create/variant-task", method = RequestMethod.POST)
    public ResponseEntity<TaskResponse> createVariantTask(
            @RequestParam(value="question", required =true) String question,
            @RequestParam(value="answer", required =true) String answer,
            @RequestParam(value="cost", required =false) Integer cost,
            @RequestParam(value="variants", required =true) String variants
    ){

        VariantsTask variantsTask = new VariantsTask(question,  answer,  cost, variants);
        simpleTaskRepository.save(variantsTask);

        return new ResponseEntity<TaskResponse>(new TaskResponse( "success", "Task was created", variantsTask), HttpStatus.OK);
    }

    @Transactional
    @RequestMapping(value = "show/variant-task", method = RequestMethod.GET)
    public ResponseEntity<TaskListResponse> getAllVariants(){
        List<VariantsTask> variantsTaskList = taskService.getVariantTasks().stream().map(simpleTask -> (VariantsTask)simpleTask).collect(Collectors.toList());

        return new ResponseEntity<TaskListResponse>(new TaskListResponse( variantsTaskList, null, "success", ""), HttpStatus.OK);
    }

    @Transactional
    @RequestMapping(value = "show/string-task", method = RequestMethod.GET)
    public ResponseEntity<TaskListResponse> getAllStrings(){
        List<StringTask> stringTaskList = taskService.getStringTasks().stream().map(simpleTask -> (StringTask)simpleTask).collect(Collectors.toList());

        return new ResponseEntity<TaskListResponse>(new TaskListResponse( stringTaskList, null,"success", ""), HttpStatus.OK);
    }

    @Transactional
    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public ResponseEntity<TaskListResponse> getAllStrings(@PathVariable Long id) {
        SimpleTask simpleTask = (SimpleTask) (simpleTaskRepository.findById(id).orElse(null));

        if (simpleTask == null)
            return new ResponseEntity<TaskListResponse>(new TaskListResponse(null, null, "error", String.format("Task with id %d doesn't exists", id)), HttpStatus.I_AM_A_TEAPOT);

        else if (simpleTask.getCategory().equals("string")) {
            simpleTaskRepository.delete(simpleTask);
            return new ResponseEntity<TaskListResponse>(new TaskListResponse(List.of(simpleTask), null, "success", "Task was deleted"), HttpStatus.OK);
        } else if (simpleTask.getCategory().equals("variant")) {
            simpleTaskRepository.delete(simpleTask);
            return new ResponseEntity<TaskListResponse>(new TaskListResponse(List.of(simpleTask), null, "success", "Task was deleted"), HttpStatus.OK);
        } else
            return new ResponseEntity<TaskListResponse>(new TaskListResponse(null, null, "error", "Bad data in database"), HttpStatus.I_AM_A_TEAPOT);




    }

}
