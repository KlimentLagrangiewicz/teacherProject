package com.usefulutils.vteacher.controller;

import com.usefulutils.vteacher.model.entity.Customer;
import com.usefulutils.vteacher.model.response.CustomerResponse;
import com.usefulutils.vteacher.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@Controller
@RequestMapping(value = "customer")
public class CustomerController {

    @Autowired
    CustomerRepository customerRepository;


    private String generatePassword(String password){
        if(password == null){
            return String.valueOf(Math.round(Math.random()*100000));
        }
        else return password;
    }

    private Date parseDate(String dob) throws Exception{

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
        try {
            return formatter.parse(dob);
        }catch (Exception e){
            return formatter.parse("11-11-1999");
        }
    }

    @Transactional
    @RequestMapping(value = "create", method = RequestMethod.POST)
    public ResponseEntity<CustomerResponse> create(
            @RequestParam(value="name", required =false) String name,
            @RequestParam(value="familyName", required =false) String familyName,
            @RequestParam(value="dob", required =false) String dob,
            @RequestParam(value="login", required =true) String login,
            @RequestParam(value="password", required =false) String password
    ){
        Customer customer = customerRepository.findById(login).orElse(null);
        if(customer!=null){
            return new ResponseEntity<CustomerResponse>(new CustomerResponse(customer, "rejected", "Customer already exists"), HttpStatus.I_AM_A_TEAPOT);
        }

        password = generatePassword(password);

        Date dateOfBirth;
        try {
            dateOfBirth = parseDate(dob);
        }catch (Exception e){
            return new ResponseEntity<CustomerResponse>(new CustomerResponse(null, "fatal error", "Not able to set dateOfBirth"), HttpStatus.I_AM_A_TEAPOT);
        }

        customer = new Customer(login, name, familyName, dateOfBirth, password);
        customerRepository.save(customer);
        return new ResponseEntity<CustomerResponse>(new CustomerResponse(customer, "success", "Customer was created"), HttpStatus.OK);
    }

    @Transactional
    @RequestMapping(value = "{login}", method = RequestMethod.DELETE)
    public ResponseEntity<CustomerResponse> delete(@PathVariable String login){
       Customer customer = customerRepository.findById(login).orElse(null);

        if(customer!=null){
            customerRepository.delete(customer);
             return new ResponseEntity<CustomerResponse>(new CustomerResponse(null, "success","Deleted"), HttpStatus.OK);
        }
        else
            return new ResponseEntity<CustomerResponse>(new CustomerResponse(null, "error","Nothing to delete"), HttpStatus.I_AM_A_TEAPOT);
    }

    @Transactional
    @RequestMapping(value = "change/{login}", method = RequestMethod.POST)
    public ResponseEntity<CustomerResponse> change(
            @PathVariable String login,
            @RequestParam(value="name", required =false) String name,
            @RequestParam(value="familyName", required =false) String familyName,
            @RequestParam(value="dob", required =false) String dob,
            @RequestParam(value="password", required =false) String password) {

        Customer customer = customerRepository.findById(login).orElse(null);
        if(customer==null){
            return new ResponseEntity<CustomerResponse>(new CustomerResponse(null, "rejected","Customer doesn't exists"), HttpStatus.I_AM_A_TEAPOT);
        }

        password = generatePassword(password);

        Date dateOfBirth;
        try {
            dateOfBirth = parseDate(dob);
        }catch (Exception e){
            return new ResponseEntity<CustomerResponse>(new CustomerResponse(null, "fatal error", "Not able to set dateOfBirth"), HttpStatus.I_AM_A_TEAPOT);
        }

        customer.update(name, familyName, dateOfBirth, password);
        customerRepository.save(customer);
        return new ResponseEntity<CustomerResponse>(new CustomerResponse(customer, "success", "Customer was updated"), HttpStatus.OK);

    }


}
