package com.usefulutils.vteacher.model.response;

import com.usefulutils.vteacher.model.entity.CustomerTask;
import com.usefulutils.vteacher.model.entity.SimpleTask;

import java.util.List;

public class TaskListResponse extends BaseResponse {

    private List<? extends SimpleTask> simpleTaskList;
    private List<CustomerTask> customerTaskList;

    public TaskListResponse(List<? extends SimpleTask> simpleTaskList, List<CustomerTask> customerTaskList, String success, String message) {
        super(success, message);
        this.simpleTaskList = simpleTaskList;
        this.customerTaskList = customerTaskList;

    }

    public List<? extends SimpleTask> getSimpleTaskList() {
        return simpleTaskList;
    }

    public void setSimpleTaskList(List<? extends SimpleTask> simpleTaskList) {
        this.simpleTaskList = simpleTaskList;
    }

    public List<CustomerTask> getCustomerTaskList() {
        return customerTaskList;
    }

    public void setCustomerTaskList(List<CustomerTask> customerTaskList) {
        this.customerTaskList = customerTaskList;
    }

}
