package com.usefulutils.vteacher.model.response;

import com.usefulutils.vteacher.model.entity.SimpleTask;

public class TaskResponse extends BaseResponse{

    SimpleTask task;

    public TaskResponse() {
    }

    public TaskResponse(String status, String message, SimpleTask task) {
        super(status, message);
        this.task = task;
    }


    public SimpleTask getTask() {
        return task;
    }

    public void setTask(SimpleTask task) {
        this.task = task;
    }
}
