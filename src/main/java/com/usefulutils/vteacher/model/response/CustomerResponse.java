package com.usefulutils.vteacher.model.response;

import com.usefulutils.vteacher.model.entity.Customer;

public class CustomerResponse extends BaseResponse {

    private Customer customer;

    public CustomerResponse() {
        super();
    }

    public CustomerResponse(Customer customer, String status, String message) {
        super(status, message);
        this.customer = customer;

    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}
