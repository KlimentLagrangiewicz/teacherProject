package com.usefulutils.vteacher.model.response;

import com.usefulutils.vteacher.model.entity.CustomerExam;

public class ExamResponse extends BaseResponse{

    CustomerExam customerExam;

    public ExamResponse(CustomerExam customerExam) {
        this.customerExam = customerExam;
    }

    public ExamResponse(String status, String message, CustomerExam customerExam) {
        super(status, message);
        this.customerExam = customerExam;
    }

    public ExamResponse() {
    }

    public CustomerExam getCustomerExam() {
        return customerExam;
    }

    public void setCustomerExam(CustomerExam customerExam) {
        this.customerExam = customerExam;
    }
}
