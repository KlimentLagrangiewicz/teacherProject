package com.usefulutils.vteacher.model.entity;

import javax.persistence.*;

@Entity
@Table(name = "customer_task", schema = "teacher")
public class CustomerTask extends BaseEntity{


    @ManyToOne(fetch = FetchType.LAZY)
    private CustomerExam customerExam;

    @OneToOne()
    @JoinColumn(name = "task_id", referencedColumnName = "id")
    private SimpleTask task;

    private String customerAnswer;
    private Boolean checkAnswer;

    public CustomerTask() {
    }

    public CustomerTask(SimpleTask task, String customerAnswer) {
        this.task = task;
        this.customerAnswer = customerAnswer;
        setCheckAnswer();
    }

    public CustomerTask(Long id) {
        super(id);
    }

    public CustomerTask(Long id, SimpleTask task, String customerAnswer, Boolean checkAnswer) {
        super(id);
        this.task = task;
        this.customerAnswer = customerAnswer;
        this.checkAnswer = checkAnswer;

    }

    public CustomerTask(SimpleTask task, CustomerExam customerExam) {
        this.task = task;
        this.customerExam = customerExam;
    }


    public SimpleTask getTask() {
        return task;
    }

    public void setTask(SimpleTask task) {
        this.task = task;
    }

    public String getCustomerAnswer() {
        return customerAnswer;
    }

    public void setCustomerAnswer(String customerAnswer) {
        this.customerAnswer = customerAnswer;
    }

    public Boolean getCheckAnswer() {
        return checkAnswer;
    }

    public void setCheckAnswer() {
        this.checkAnswer = this.task.getAnswer().equals(this.customerAnswer);
    }


    @Override
    public String toString() {
        return "CustomerTask{" +
                "id=" + id +
                ", task=" + task +
                ", customerAnswer='" + customerAnswer + '\'' +
                ", checkAnswer=" + checkAnswer +
                '}';
    }
}
