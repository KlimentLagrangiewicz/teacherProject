package com.usefulutils.vteacher.model.entity;


import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "customer_exam", schema = "teacher")
public class CustomerExam extends BaseEntity{

    @OneToMany(mappedBy = "customerExam",cascade = CascadeType.ALL, orphanRemoval = true)
    List<CustomerTask> listTask;
    Date dueDate;
    Integer points;
    String mark;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "customer", referencedColumnName = "login")
    Customer customer;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "exam_type_id", referencedColumnName = "id")
    ExamType examType;

    public CustomerExam() {
    }

    public CustomerExam(Long id) {
        super(id);
    }

    public CustomerExam(List<CustomerTask> listTask, Date dueDate, Integer points, String mark, Customer customer, ExamType examType) {
        this.listTask = listTask;
        this.dueDate = dueDate;
        this.points = points;
        this.mark = mark;
        this.customer = customer;
        this.examType = examType;
    }

    public CustomerExam(Long id, List<CustomerTask> listTask, Date dueDate, Integer points, String mark, Customer customer) {
        super(id);
        this.listTask = listTask;
        this.dueDate = dueDate;
        this.points = points;
        this.mark = mark;
        this.customer = customer;
    }

    public CustomerExam(Long id, List<CustomerTask> listTask, Date dueDate, Integer points, String mark, Customer customer, ExamType examType) {
        super(id);
        this.listTask = listTask;
        this.dueDate = dueDate;
        this.points = points;
        this.mark = mark;
        this.customer = customer;
        this.examType = examType;
    }


    public List<CustomerTask> getListTask() {
        return listTask;
    }

    public void setListTask(List<CustomerTask> listTask) {
        this.listTask = listTask;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public ExamType getExamType() {
        return examType;
    }

    public void setExamType(ExamType examType) {
        this.examType = examType;
    }
}
