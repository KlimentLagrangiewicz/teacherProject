package com.usefulutils.vteacher.model.entity;


import org.springframework.context.annotation.Primary;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "customer", schema = "teacher")
public class Customer {

    @Id
    @Column(name = "login")
    String login;

    String name;
    String familyName;
    Date dob;
    String password;

    public Customer() {
    }

    public Customer(String login, String name, String familyName, Date dob, String password) {
        this.login = login;
        this.name = name;
        this.familyName = familyName;
        this.dob = dob;
        this.password = password;
    }

    public void update(String name, String familyName, Date dob, String password){
        this.name = name;
        this.familyName = familyName;
        this.dob = dob;
        this.password = password;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getLogin() {
        return login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
