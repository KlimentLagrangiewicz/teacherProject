package com.usefulutils.vteacher.model.entity;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import javax.persistence.*;

@Entity
@Table(name = "tasks", schema = "teacher")
@DiscriminatorColumn(name = "category")
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property = "category")
@JsonSubTypes({ @JsonSubTypes.Type(value = VariantsTask.class, name = "variant"),
        @JsonSubTypes.Type(value = StringTask.class, name = "string") })
public abstract class SimpleTask extends BaseEntity {

    @Column(insertable = false, updatable = false)
    private String category;
    private Integer cost;
    private String question;
    private String answer;

    public SimpleTask() { super(); }

    public SimpleTask(long id) {
        super(id);
    }

    public SimpleTask(Long id, String category, Integer cost, String question, String answer) {
        super(id);
        this.category = category;
        this.cost = cost;
        this.question = question;
        this.answer = answer;
    }

    public SimpleTask(String question, String answer, Integer cost) {
        this.question = question;
        this.answer = answer;
        this.cost = cost;
    }


    public Integer getCost() {
        return cost;
    }

    public String getCategory() {
        return category;
    }

    public void setCost(Integer cost) {

    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    @Override
    public String toString() {
        return "SimpleTask{" +
                "id=" + id +
                ", category='" + category + '\'' +
                ", cost=" + cost +
                ", question='" + question + '\'' +
                ", answer='" + answer + '\'' +
                '}';
    }
}
