package com.usefulutils.vteacher.model.entity;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Map;

@Entity
@Table(name = "exam_types", schema = "teacher")
public class ExamType extends BaseEntity {

    String examMarksMap;
    Integer examTimeMinutes;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    @Transient
    Map<String, Integer> prettyMarkMap;

    public ExamType() {
    }

    public ExamType(String examMarksMap, Integer examTimeMinutes) {
        this.examMarksMap = examMarksMap;
        this.examTimeMinutes = examTimeMinutes;
    }

    public String getExamMarkMap() {
        return examMarksMap;
    }

    public void setExamMarkMap(String examMarkMap) {
        this.examMarksMap = examMarkMap;
    }

    public Integer getExamTimeMinutes() {
        return examTimeMinutes;
    }

    public void setExamTimeMinutes(Integer examTimeMinutes) {
        this.examTimeMinutes = examTimeMinutes;
    }

    public Map<String, Integer> getPrettyMarkMap() {
        return prettyMarkMap;
    }

    public void setPrettyMarkMap(Map<String, Integer> prettyMarkMap) {
        this.prettyMarkMap = prettyMarkMap;
    }
}
