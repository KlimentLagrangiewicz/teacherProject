package com.usefulutils.vteacher.model.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("variant")
public class VariantsTask extends SimpleTask{

    String variants;


    public VariantsTask() {
    }

    public VariantsTask(String question, String answer, Integer cost, String variants) {
        super(question, answer, cost);
        this.variants = variants;
    }

    public String getVariants() {
        return variants;
    }

    public void setVariants(String variants) {
        this.variants = variants;
    }
}
