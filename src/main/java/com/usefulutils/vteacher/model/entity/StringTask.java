package com.usefulutils.vteacher.model.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("string")
public class StringTask extends SimpleTask{

    public StringTask(String question, String answer, Integer cost) {
        super(question, answer, cost);
    }

    public StringTask() {
    }
}
